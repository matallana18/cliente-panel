import { Injectable } from '@angular/core';

export interface BadgeItem {
    type: string;
    value: string;
}
export interface Saperator {
    name: string;
    type?: string;
}
export interface SubChildren {
    state: string;
    name: string;
    type?: string;
}
export interface ChildrenItems {
    state: string;
    name: string;
    type?: string;
    child?: SubChildren[];
}

export interface Menu {
    state: string;
    name: string;
    type: string;
    icon: string;
    badge?: BadgeItem[];
    saperator?: Saperator[];
    children?: ChildrenItems[];
}

const MENUITEMS = [
    {
        state: 'dashboards',
        name: 'Dashboards',
        type: 'sub',
        icon: 'av_timer',
        children: [
            { state: 'dashboard', name: 'Dashboard', type: 'link' }
        ]
    },
    {
        state: 'apps',
        name: 'Aplicaciones',
        type: 'sub',
        icon: 'apps',
        badge: [{ type: 'warning', value: 'new' }],
        children: [
            { state: 'calendar', name: 'Calendar', type: 'link' },
            { state: 'messages', name: 'Mail box', type: 'link' },
            { state: 'taskboard', name: 'Taskboard', type: 'link' },
            { state: 'notes', name: 'Notes', type: 'link' },
            { state: 'employeelist', name: 'Employees', type: 'link' }
        ]
    },
    {
        state: 'charts',
        name: 'Graficas',
        type: 'sub',
        icon: 'insert_chart',
        children: [
            { state: 'ngxchart', name: 'Ngx Charts', type: 'link' }
        ]
    },
    {
        state: 'pages',
        name: 'Paginas',
        type: 'sub',
        icon: 'content_copy',
        children: [
            { state: 'invoice', name: 'Invoice', type: 'link' },

        ]
    },
    
];

@Injectable()
export class MenuItems {
    getMenuitem(): Menu[] {
        return MENUITEMS;
    }
}
