import { Component} from '@angular/core';
import { MyserviceService } from '../services/auth/myservice.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs/internal/Observable';
import { Subscription } from 'rxjs/internal/Subscription';

import {
  FormGroup,
  FormControl
} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [MyserviceService]
})


export class LoginComponent  {
  msg = '';
  constructor(private s_auth: MyserviceService, 
              private router: Router,
              private matSack: MatSnackBar,
              private matSack2: MatSnackBar) {

    if (localStorage.getItem('token')) {
      this.router.navigateByUrl('/dashboards/dashboard')
    } else {
      this.router.navigate(['/login']);
      return;
    }
  }

  obs_error: Observable<any>

  obs_login: Subscription

  obs_keysErrors: Observable<any>

  private cleanObs(): void {
    setTimeout(() => {
      this.obs_keysErrors = new Observable(obs => {
        obs.next([])
      });
    }, 5000);
  }

  private get_start_login(): void {
    this.obs_login = this.s_auth.login().subscribe(
      data  => {
        this.router.navigate(['/dashboards/dashboard']);
      },
      error => {
        this.matSack2.open(`${error.title}`, null, { horizontalPosition: 'right', verticalPosition: 'top' })
       // this.form_login.reset();
        this.obs_keysErrors = new Observable((obs => {
          obs.next(error.validations)
          obs.complete()
        }))
        setTimeout(() => {
          this.matSack.open(`${error.snack_error}`)
        }, 1000);
        this.cleanObs();
      },
      () => {
      }
    )
  }
  login(event: Event): void {
    if (event.type === 'submit') {
      if (this.form_login.valid) {
        this.get_start_login()
        console.log(this.form_login)
      }
    }
  }

    /** controls forms */
    get form_login(): FormGroup {
      return this.s_auth.form_login as FormGroup;
    }
  
    get email(): FormControl {
      return this.s_auth.form_login.get('email') as FormControl;
    }
  
    get password(): FormControl {
      return this.s_auth.form_login.get('password') as FormControl;
    }
}
