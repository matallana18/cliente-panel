import { Component } from '@angular/core';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent {
  invoiceItems: any = [
    {
      title: 'Vecinapp.cl',
      subtitle: 'Vecinapp.cl',
      price: 499.0,
      quantity: 1
    },
    {
      title: 'Vecinapp.cl',
      subtitle: 'Vecinapp.cl',
      price: 399.0,
      quantity: 1
    },
    {
      title: 'Admin Wrap',
      subtitle: 'Vecinapp.cl',
      price: 599.0,
      quantity: 1
    },
    {
      title: 'Admin Wrap',
      subtitle: 'Vecinapp.cl',
      price: 0.0,
      quantity: 1
    }
  ];

  invoiceTotals: any = [
    {
      subtotal: this.getSubTotal(),
      tax: this.getCalculatedTax(),
      discount: 0.0,
      total: 0
    }
  ];

  getSubTotal() {
    let total = 0.0;
    for (let i = 1; i < this.invoiceItems.length; i++) {
      total += this.invoiceItems[i].price * this.invoiceItems[i].quantity;
    }
    return total;
  }

  getCalculatedTax() {
    return (2 * this.getSubTotal()) / 100;
  }

  getTotal() {
    return this.getSubTotal() + this.getCalculatedTax();
  }

  constructor() {}
}
