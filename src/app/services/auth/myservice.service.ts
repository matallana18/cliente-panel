import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Injectable()

export class MyserviceService {
  
form_login: FormGroup;
private path: Readonly<string> = 'https://vecin.app/api/inv-v1/'

  constructor(private httpc: HttpClient, 
            private fb: FormBuilder) {   
      this.init_form_login();
}


private handleError(error: HttpErrorResponse) {
  return throwError({
    snack_error: error.error.name,
    title: `${error.error.from}, ${error.error.message}`,
    validations: error.error.group_messages
  })
}

private headers(): HttpHeaders {
  const header: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${localStorage.getItem('token')}`
  });
  console.log(header)
  return header;
}


private init_form_login(): void {
  this.form_login = this.fb.group({
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required, Validators.minLength(8)])
  });
}


login = () => {
  return this.httpc.post(`${this.path}auth/login`, this.form_login.value, { headers: this.headers() })
  .pipe(
    catchError(this.handleError),
    map( (res: any) => {
    console.log(JSON.stringify(res.messageLogin.token))
    localStorage.setItem('token', res.messageLogin.token)
    return res
  }))
} 


}
