FROM node:14.3.0  As builder

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build --prod

FROM nginx:1.19
  
COPY --from=builder /usr/src/app/dist/ /usr/share/nginx/html

RUN rm -rf /etc/nginx/conf.d/default.conf

EXPOSE 80
EXPOSE 443

CMD ["nginx", "-g", "daemon off;"]